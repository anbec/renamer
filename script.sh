#!/bin/bash

for i in [L,l]ezione_??_??_????.pdf ; do
  # Saves the day
    tmp=${i#[L,l]ezione_}
    day=${tmp%_??_201?.pdf}
    
  # Saves the month
    tmp=${tmp#??_}
    month=${tmp%_201?.pdf}
    
  # Saves the year
    tmp=${tmp#??_}
    year=${tmp%.pdf}
  
  #Modifies the file name
  new=$year$month$day.pdf
  mv $i $new
done